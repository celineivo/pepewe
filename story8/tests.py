import time

from django.test import TestCase, Client, LiveServerTestCase

from selenium import webdriver

from selenium.webdriver.chrome.options import Options

import os


# Create your tests here.

class LandingPageUnitTest(TestCase):
    def test_landing_page_found(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)


class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(LandingPageFunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

        super(LandingPageFunctionalTest, self).tearDown()

    def test_check_accordion_click_show_data(self):
        aktivitas_keyword = ['PPW', 'Film', 'Game']
        panitia_keyword = ['PERAK', 'Compfest', 'USC']
        prestasi_keyword = ['Found']
        biodata_keyword = ['Celine']

        # self.browser.get(self.live_server_url + '/story8')
        self.browser.get('http://localhost:8000/story8')
        time.sleep(5)
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        activity_button = self.browser.find_element_by_xpath(
            "//div[@class='accordion-header-left'][contains(.,'Aktivitas')][1]")
        activity_button.click()

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in aktivitas_keyword:
            xpath_statement += "[contains(.,'" + keyword + "')]"

        xpath_statement += "[1]"

        activity_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(activity_body.is_displayed())

        organizational_button = self.browser.find_element_by_xpath(
            "//div[@class='accordion-header-left'][contains(.,'Organisasi / Kepanitiaan')][1]")
        organizational_button.click()
        time.sleep(5)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in panitia_keyword:
            xpath_statement += "[contains(.,'" + keyword + "')]"

        xpath_statement += "[1]"

        organizational_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(organizational_body.is_displayed())

        achievement_button = self.browser.find_element_by_xpath(
            "//div[@class='accordion-header-left'][contains(.,'Prestasi')][1]")
        achievement_button.click()
        time.sleep(5)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in prestasi_keyword:
            xpath_statement += "[contains(.,'" + keyword + "')]"

        xpath_statement += "[1]"

        achievement_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(achievement_body.is_displayed())

        achievement_button = self.browser.find_element_by_xpath(
            "//div[@class='accordion-header-left'][contains(.,'Biodata')][1]")
        achievement_button.click()
        time.sleep(5)

        xpath_statement = "//div[@class='accordion-body']"

        for keyword in biodata_keyword:
            xpath_statement += "[contains(.,'" + keyword + "')]"

        xpath_statement += "[1]"

        contact_me_body = self.browser.find_element_by_xpath(xpath_statement)
        self.assertTrue(contact_me_body.is_displayed())

    def test_up_and_down_accordion(self):
        # self.browser.get(self.live_server_url + '/')
        self.browser.get('http://localhost:8000/story8')
        up_activity_button = self.browser.find_element_by_xpath(
            "//div[@class='accordion shadow aos-init aos-animate'][contains(.,'Aktivitas')]/div[1]/div[2]/div[1]")
        down_activity_button = self.browser.find_element_by_xpath(
            "//div[@class='accordion shadow aos-init aos-animate'][contains(.,'Aktivitas')]/div[1]/div[2]/div[2]")

        page_source = self.browser.page_source

        self.assertTrue(page_source.find('Aktivitas') < page_source.find('Organisasi / Kepanitiaan'))

        down_activity_button.click()
        time.sleep(5)
        page_source = self.browser.page_source

        self.assertTrue(page_source.find('Aktivitas') > page_source.find('Organisasi / Kepanitiaan'))

        up_organizational_button = self.browser.find_element_by_xpath(
            "//div[@class='accordion shadow aos-init aos-animate'][contains(.,'Organisasi / Kepanitiaan')]/div[1]/div[2]/div[1]")
        down_organizational_button = self.browser.find_element_by_xpath(
            "//div[@class='accordion shadow aos-init aos-animate'][contains(.,'Organisasi / Kepanitiaan')]/div[1]/div[2]/div[2]")

        down_organizational_button.click()
        page_source = self.browser.page_source

        self.assertTrue(page_source.find('Aktivitas') < page_source.find('Organisasi / Kepanitiaan'))

    def test_story8_color_change(self):
        # self.browser.get(self.live_server_url + '/story8/')
        self.browser.get('http://localhost:8000/story8')
        time.sleep(2)
        bgcolor = self.browser.find_element_by_css_selector("body")
        color_before = bgcolor.value_of_css_property("background-color")

        button_mod = self.browser.find_element_by_id("styleModifier")

        button_mod.click()

        bgcolor = self.browser.find_element_by_css_selector("body")
        color_after = bgcolor.value_of_css_property("background-color")
        self.assertNotEqual(color_before, color_after)


