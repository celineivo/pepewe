from django.shortcuts import render


# Create your views here.

def homepage(request):
    return render(request, 'homepage.html')


def story8Views(request):
    return render(request, 'story8.html')
