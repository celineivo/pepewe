from django.urls import path
from . import views

urlpatterns = [
    path('', views.story7),
    path('post/', views.post_data),
    path('post/success/', views.confirm_status)
]