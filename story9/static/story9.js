var bookShowed;

if (localStorage.getItem('likedBook') != null) {
    var jsonLikedBook = JSON.parse(localStorage.getItem('likedBook'));
    likedBook = jsonLikedBook['likedBook'];
} else {
    likedBook = [];
    var jsonLikedBook = {'likedBook': likedBook};
    localStorage.setItem('likedBook', JSON.stringify(jsonLikedBook));
}

$(document).ready(function () {
    $('#bookQuery').on('input', function () {
        var query = $(this).val();
        if (query.length > 3) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    $('.book-result-container').html('');
                    var data = JSON.parse(this.responseText);
                    bookShowed = data['items']
                    var bookHTML;
                    for (var i = 0; i < data['items'].length; i++) {
                        var book = data['items'][i];
                        bookHTML = '';
                        bookHTML += '<div class="book-item">';
                        if ('imageLinks' in book['volumeInfo']) {
                            bookHTML += '<img class="book-thumbnail" src="' + book['volumeInfo']['imageLinks']['thumbnail'] + '">';
                        }
                        bookHTML += '<div class="book-detail">';
                        bookHTML += '<div class="book-title">' + book['volumeInfo']['title'] + '</div>';
                        bookHTML += '<div class="book-publisher">' + book['volumeInfo']['publisher'] + '</div>';
                        bookHTML += '<button type="button" class="book-like-button" onclick="saveBook(this, ' + i + ')">';
                        if (likedBook.includes(book['id'])) {
                            bookHTML += '<i class="fas fa-heart"></i> <span class="like-text"></span>';
                        } else {
                            bookHTML += '<i class="far fa-heart"></i> <span class="like-text"></span>';
                        }

                        bookHTML += '</button></div></div>';
                        $('#bookResult').append(bookHTML);
                    }
                }
            };
            xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + query, true);
            xhttp.send();
        }

    });
});

function queryCheck() {
    var query = $('#bookQuery').val();
    if (query != '' && query.length > 3 && query != queryCheckSum) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    queryCheckSum = query;
                    $('.book-result-container').html('');
                    var data = JSON.parse(this.responseText);
                    if ('items' in data) {
                        bookShowed = data['items']
                        var bookHTML;
                        for (var i = 0; i < data['items'].length; i++) {
                            var book = data['items'][i];
                            bookHTML = '';
                            bookHTML += '<div class="book-item">';
                            if ('imageLinks' in book['volumeInfo']) {
                                bookHTML += '<img class="book-thumbnail" src="' + book['volumeInfo']['imageLinks']['thumbnail'] + '">';
                            }
                            bookHTML += '<div class="book-detail">';
                            bookHTML += '<div class="book-title">' + book['volumeInfo']['title'] + '</div>';
                            bookHTML += '<div class="book-publisher">' + book['volumeInfo']['publisher'] + '</div>';
                            bookHTML += '<button type="button" class="book-like-button" onclick="saveBook(this, ' + i + ')">';
                            if (likedBook.includes(book['id'])) {
                                bookHTML += '<i class="fas fa-heart"></i> <span class="like-text"></span>';
                            } else {
                                bookHTML += '<i class="far fa-heart"></i> <span class="like-text"></span>';
                            }
                            bookHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
                            bookHTML += '</button></div></div>';
                            $('#bookResult').append(bookHTML);
                        }
                    } else {
                        $('.book-result-container').html('No result for <b>"' + query + '"</b>');
                    }
                }
            }
        };
        xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + query, true);
        xhttp.send();
    }
}


// Like book from showed book and ajax request to django
function saveBook(e, bookIndex) {
    var bookData = bookShowed[bookIndex];
    // Prevent multiple like for each user (browser)
    if (likedBook.includes(bookData['id'])) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            $(e).children('.lds-ring').css('display', 'inline-block');
            $(e).children('.like-text').css('display', 'none');
            if (this.readyState == 4) {
                $(e).children('.lds-ring').css('display', 'none');
                $(e).children('.like-text').css('display', 'inline-block');
                if (this.status == 200) {
                    e.innerHTML = '<i class="far fa-heart"></i> <span class="like-text"></span>';
                    e.innerHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
                    var savedBookIndex = likedBook.indexOf(bookData['id'])
                    likedBook.splice(savedBookIndex, 1);
                    saveLocalData();
                    loadBooksLoved();
                } else {
                    e.innerHTML = 'Error unlike book';
                }
            }
        };
        xhttp.open("POST", "story9/api/v1/unlikebook/", true);

        var bookDataJSON = JSON.stringify(bookData);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(bookDataJSON);
    } else {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            $(e).children('.lds-ring').css('display', 'inline-block');
            $(e).children('.like-text').css('display', 'none');
            if (this.readyState == 4) {
                $(e).children('.lds-ring').css('display', 'none');
                $(e).children('.like-text').css('display', 'inline-block');
                if (this.status == 200) {
                    e.innerHTML = '<i class="fas fa-heart"></i> <span class="like-text"></span>';
                    e.innerHTML += '<div class="lds-ring"><div></div><div></div><div></div>';
                    likedBook.push(bookData['id']);
                    saveLocalData();
                    loadBooksLoved();
                } else {
                    e.innerHTML = 'Error like book';
                }
            }
        };
        xhttp.open("POST", "story9/api/v1/likebook/", true);

        var bookDataJSON = JSON.stringify(bookData);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(bookDataJSON);
    }
}


var topBookShowed = []

function loadTopBook() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            topBook = []
            $('#topBookContainer').html('');
            var data = JSON.parse(this.responseText);
            var topBook = data['topBook'];
            var bookHTML;
            for (var i = 0; i < topBook.length; i++) {
                var book = topBook[i];
                topBookShowed.push(book['bookId']);
                bookHTML = '';
                bookHTML += '<div class="book-item">';
                bookHTML += '<img class="book-thumbnail" src="' + book['imageLinks'] + '">';
                bookHTML += '<div class="book-detail">';
                bookHTML += '<div class="book-title">' + book['title'] + '</div>';
                bookHTML += '<div class="book-publisher">' + book['publisher'] + '</div>';
                bookHTML += '<button type="button" class="book-like-button" onclick="likeTopBook(this, ' + i + ')">';
                bookHTML += book['likeCount'] + ' <i class="far fa-heart"></i>';
                bookHTML += '</button>';
                bookHTML += '</div></div>';
                $('#topBookContainer').append(bookHTML);
            }
        }
    };
    xhttp.open("GET", "story9/api/v1/topbook/", true);
    xhttp.send();
}


function likeTopBook(e, bookIndex) {
    if (likedBook.includes(topBookShowed[bookIndex])) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            $(e).children('.lds-ring').css('display', 'inline-block');
            $(e).children('.like-text').css('display', 'none');
            if (this.readyState == 4) {
                $(e).children('.lds-ring').css('display', 'none');
                $(e).children('.like-text').css('display', 'inline-block');
                if (this.status == 200) {
                    likeBookCount = JSON.parse(this.responseText)['likeCount'];
                    e.innerHTML = likeBookCount + '  <i class="far fa-heart"></i> <span class="like-text"></span>';
                    var savedBookIndex = likedBook.indexOf(topBookShowed[bookIndex])
                    likedBook.splice(savedBookIndex, 1);
                    saveLocalData();
                    loadBooksLoved();
                } else {
                    e.innerHTML = 'Error unlike book';
                }
            }
        };
        xhttp.open("POST", "story9/api/v1/unlikebook/", true);
        var bookData = {
            'id': topBookShowed[bookIndex]
        };
        var bookDataJSON = JSON.stringify(bookData);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(bookDataJSON);
    } else {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            $(e).children('.lds-ring').css('display', 'inline-block');
            $(e).children('.like-text').css('display', 'none');
            if (this.readyState == 4) {
                $(e).children('.lds-ring').css('display', 'none');
                $(e).children('.like-text').css('display', 'inline-block');
                if (this.status == 200) {
                    likeBookCount = JSON.parse(this.responseText)['likeCount'];
                    e.innerHTML = likeBookCount + '  <i class="fas fa-heart"></i> <span class="like-text"></span>';
                    likedBook.push(topBookShowed[bookIndex]);
                    saveLocalData();
                    loadBooksLoved();
                } else {
                    e.innerHTML = 'Error like book';
                }
            }
        };
        xhttp.open("POST", "story9/api/v1/likebook/", true);
        var bookData = {
            'id': topBookShowed[bookIndex]
        };
        var bookDataJSON = JSON.stringify(bookData);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(bookDataJSON);
    }
}


function saveLocalData() {
    localStorage.setItem('likedBook', JSON.stringify({'likedBook': likedBook}));
}
