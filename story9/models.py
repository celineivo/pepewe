from django.db import models


# Create your models here.

class Books(models.Model):
    book_id = models.CharField(max_length=64, unique=True)
    title = models.CharField(max_length=128)
    publisher = models.CharField(max_length=128, null=True)
    published_date = models.CharField(max_length=32, null=True)
    like_count = models.IntegerField(default=0)
    image_link = models.TextField()
    authors = models.CharField(max_length=64, null=True)
    average_rating = models.IntegerField(null=True)

    def __str__(self):
        return self.title + " - " + self.book_id
