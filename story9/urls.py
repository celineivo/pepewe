from django.urls import path
from . import views

urlpatterns = [
    path('', views.story9Views),
    path('story9/api/v1/likebook/', views.api_like_book, name="api_like_book"),
    path('story9/api/v1/topbook/', views.top_book, name="top_books"),
    path('story9/api/v1/unlikebook/', views.api_unlike_book, name="api_unlike_book"),
]
