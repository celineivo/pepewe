import random
import string

from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import os
import time

from story9.models import Books


class LandingPageUnitTest(TestCase):
    def test_landing_page_found(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_add_books_to_database(self):
        book_id = 'ajHIkjyokW'
        like_count = 5
        title = "TOEFL Test 2020"
        publisher = "Roy Simatupang"
        Books.objects.create(
            book_id=book_id,
            title=title,
            publisher=publisher,
            published_date=None,
            like_count=like_count,
            image_link='',
            authors=None,
            average_rating=None
        )

        data = Books.objects.get(title=title)
        self.assertEqual(str(data), title + ' - ' + book_id)

    def test_model_string_return(self):
        letters = string.ascii_letters
        random_book_id = ''.join(random.choice(letters) for i in range(10))
        random_like_count = random.randint(1, 10)
        title = "Love"
        publisher = "Fawcett"
        Books.objects.create(
            book_id=random_book_id,
            title=title,
            publisher=publisher,
            published_date=None,
            like_count=random_like_count,
            image_link='',
            authors=None,
            average_rating=None
        )

        data = Books.objects.get(title=title)
        self.assertEqual(str(data), title + ' - ' + random_book_id)


class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(LandingPageFunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

        super(LandingPageFunctionalTest, self).tearDown()

    def test_title_and_search_box_exist_in_homepage(self):
        self.browser.get(self.live_server_url + '/story9')
        # self.browser.get('http://localhost:8000/story9')

        time.sleep(10)

        self.assertIn("Books", self.browser.title)

        page_text = self.browser.find_element_by_class_name('main-title').text
        self.assertIn('books', page_text)

    def test_search_box_show_books_with_name(self):
        self.browser.get(self.live_server_url + '/story9')
        # self.browser.get('http://localhost:8000/story9')
        time.sleep(5)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        query_input.send_keys("Love Fawcett")
        time.sleep(5)  # Wait web page to process and showing result

        book_item = self.browser.find_element_by_class_name('book-item')
        book_data = book_item.get_attribute('innerHTML')

        self.assertIn('Love', book_data)

        book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(10)
        liked_book_data = Books.objects.get(title=book_title)
        self.assertEqual(liked_book_data.like_count, 1)

    def test_unlike_book(self):
        self.browser.get(self.live_server_url + '/story9')
        # self.browser.get('http://localhost:8000/story9')
        time.sleep(15)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        query_input.send_keys("Love Fawcett")
        time.sleep(2)
        query_input.send_keys(" ")
        time.sleep(15)

        book_title = self.browser.find_element_by_class_name('book-title').get_attribute('innerHTML')
        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)

        books = Books.objects.get(title=book_title)
        self.assertEqual(books.like_count, 1)

        book_like_button.click()
        time.sleep(5)

        books = Books.objects.get(title=book_title)
        self.assertEqual(books.like_count, 0)

    def test_books_without_full_data(self):
        self.browser.get(self.live_server_url + '/story9')
        # self.browser.get('http://localhost:8000/story9')

        time.sleep(15)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        time.sleep(15)
        query_input.send_keys("Bluetongue - AGDPT, CFT, Microtitre Neutralisation Test")
        time.sleep(15)
        query_input.send_keys(" ")
        time.sleep(15)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)

        books = Books.objects.get(title='Bluetongue - AGDPT, CFT, Microtitre Neutralisation Test')

        self.assertTrue(books.like_count, 1)

        query_input.clear()
        query_input.send_keys("Counsel to Ladies and Easy-going Men on Their Business Investments")
        query_input.send_keys(" ")
        time.sleep(15)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)

        books = Books.objects.get(title='Counsel to Ladies and Easy-going Men on Their Business Investments')

        self.assertTrue(books.like_count, 1)

        query_input.clear()
        query_input.send_keys("Manajemen Fit & Proper Test")
        query_input.send_keys(" ")
        time.sleep(15)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)

        books = Books.objects.get(title='Manajemen Fit & Proper Test')

        self.assertTrue(books.like_count, 1)
        self.browser.execute_script("localStorage.clear();")

        self.browser.get(self.live_server_url + '/story9')
        # self.browser.get('http://localhost:8000/story9')
        time.sleep(5)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        query_input.send_keys("Manajemen Fit & Proper Test")
        query_input.send_keys(" ")
        time.sleep(15)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)

        books = Books.objects.get(title='Manajemen Fit & Proper Test')

        self.assertTrue(books.like_count, 2)
    
    def test_top5_liked_books(self):
        self.browser.get(self.live_server_url + '/story9')
        
        time.sleep(5)

        query_input = self.browser.find_element_by_xpath("//input[@id='bookQuery'][1]")
        time.sleep(10)
        query_input.send_keys("Counsel to Ladies and Easy-going Men on Their Business Investments")
        time.sleep(10)
        query_input.send_keys(" ")
        time.sleep(10)

        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(5)

        query_input.clear()

        time.sleep(10)

        book_top5_button = self.browser.find_element_by_class_name('top5-like-button')
        book_top5_button.click()
        time.sleep(5)
        book_item = self.browser.find_element_by_class_name('book-item')
        book_item_html = book_item.get_attribute('innerHTML')
        
        time.sleep(15)
        
        book_like_button = self.browser.find_element_by_class_name('book-like-button')

        book_like_button.click()
        time.sleep(10)

        book_found = False
        if 'Counsel to Ladies and Easy-going Men on Their Business Investments' in book_item_html:
            book_found = True

        self.assertTrue(book_found)
        
        
        

