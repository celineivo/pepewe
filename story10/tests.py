from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import os
import time


class LoginPageUnitTest(TestCase):
    def test_redirect_to_login_page(self):
        response = Client().get('/story10/')
        self.assertEqual(response.status_code, 302)

    def test_bad_request_api(self):
        response = Client().get('/story10/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/story10/api/v1/signup/')
        self.assertEqual(response.status_code, 400)


class LoginPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(LoginPageFunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            # self.browser = webdriver.Chrome(chrome_options=chrome_options)
            self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

        super(LoginPageFunctionalTest, self).tearDown()

    def test_login_page_login(self):
        # Create User
        User.objects.create_user('asd', 'story10@terakhir.com', 'testt')
        time.sleep(5)

        self.browser.get(self.live_server_url + '/story10')
        # self.browser.get('http://localhost:8000/story10')

        # Wait until page open
        time.sleep(5)

        # sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Create new account")]')
        # sign_up_button.click()
        #
        # time.sleep(5)
        #
        # # Fill details
        # username = self.browser.find_element_by_xpath('//input[@name="s_username"]')
        # username.send_keys("asd")
        #
        # email = self.browser.find_element_by_xpath('//input[@name="s_email"]')
        # email.send_keys("test@testt.com")
        #
        # password = self.browser.find_element_by_xpath('//input[@name="s_password"]')
        # password.send_keys("testt")
        #
        # # Sign up
        # sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Sign Up")]')
        # sign_up_button.click()
        #
        # time.sleep(15)

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("asd")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("testt")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        # Wait Authentication process and redirect to protected homepage
        time.sleep(10)
        self.assertIn('asd', self.browser.page_source)  # Verify username is showed

        logout_button = self.browser.find_element_by_xpath('//button[contains(.,"Logout")]')
        logout_button.click()

        time.sleep(5)
        self.assertNotIn('asd', self.browser.page_source)  # Verify logout

    def test_login_page_login_no_user(self):
        self.browser.get(self.live_server_url + '/story10')
        # self.browser.get('http://localhost:8000/story10')

        # Wait until page open
        time.sleep(5)

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("lalala")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("terakhir")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        time.sleep(15)

        # Wait Authentication process and redirect to protected homepage
        self.assertIn('Username does not exist', self.browser.page_source)  # Verify error message is showed

    def test_login_page_wrong_password(self):
        # Create User
        User.objects.create_user('asdasd', 'test@test.com', 'testt')
        time.sleep(5)

        self.browser.get(self.live_server_url + '/story10')
        # self.browser.get('http://localhost:8000/story10')

        # Wait until page open
        time.sleep(5)

        # sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Create new account")]')
        # sign_up_button.click()
        #
        # time.sleep(5)
        #
        # # Fill details
        # username = self.browser.find_element_by_xpath('//input[@name="s_username"]')
        # time.sleep(2)
        # username.send_keys("asdasd")
        #
        # email = self.browser.find_element_by_xpath('//input[@name="s_email"]')
        # time.sleep(2)
        # email.send_keys("test@test.com")
        #
        # password = self.browser.find_element_by_xpath('//input[@name="s_password"]')
        # time.sleep(2)
        # password.send_keys("testt")
        #
        # time.sleep(2)
        # # Sign up
        # sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Sign Up")]')
        # sign_up_button.click()

        # time.sleep(15)

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        time.sleep(2)
        username.send_keys("asdasd")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        time.sleep(2)
        password.send_keys("test123")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        time.sleep(15)

        self.assertIn('Wrong Password', self.browser.page_source)

    def test_create_account(self):
        self.browser.get(self.live_server_url + '/story10')
        # self.browser.get('http://localhost:8000/story10')

        # Wait until page open
        time.sleep(5)

        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Create new account")]')
        sign_up_button.click()

        time.sleep(5)

        # Fill details
        username = self.browser.find_element_by_xpath('//input[@name="s_username"]')
        time.sleep(5)
        username.send_keys("lol")

        email = self.browser.find_element_by_xpath('//input[@name="s_email"]')
        time.sleep(5)
        email.send_keys("lol@terakhir.com")

        password = self.browser.find_element_by_xpath('//input[@name="s_password"]')
        time.sleep(5)
        password.send_keys("lolol")

        # Sign up
        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Sign Up")]')
        time.sleep(5)
        sign_up_button.click()

        time.sleep(10)

        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Create new account")]')
        sign_up_button.click()

        # TEST DUPLICATE USERNAME
        username = self.browser.find_element_by_xpath('//input[@name="s_username"]')
        time.sleep(5)
        username.clear()
        username.send_keys("lol")

        email = self.browser.find_element_by_xpath('//input[@name="s_email"]')
        time.sleep(5)
        email.clear()
        email.send_keys("story@terakhir.com")

        password = self.browser.find_element_by_xpath('//input[@name="s_password"]')
        time.sleep(5)
        password.clear()
        password.send_keys("terakhir")

        # Sign up
        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Sign Up")]')
        sign_up_button.click()

        time.sleep(15)

        self.assertIn('Username is taken', self.browser.page_source)
